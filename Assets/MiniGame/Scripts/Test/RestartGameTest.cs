﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using WingsMob.MiniGame.TileCat;
using WingsMob.MiniGame.TileCat.Controller;
using WingsMob.MiniGame.TileCat.TileCat;

public class RestartGameTest : MonoBehaviour
{
    [SerializeField] List<ClickableItem> m_clickableItems;
    [SerializeField] List<Slot> m_slots;

    public void RestartGame()
    {
        Destroy(MiniGameController.Instance.gameObject);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        //foreach (var item in m_clickableItems)
        //{
        //    item.ResetItem();
        //}
        //foreach (var slot in m_slots)
        //{
        //    slot.ResetSlot();
        //}
        //for (int i = 0; i < Enum.GetNames(typeof(CatItemType)).Length; i++)
        //{
        //    MiniGameController.Instance.ClearMatchItem((CatItemType)i);
        //}
    }
}
