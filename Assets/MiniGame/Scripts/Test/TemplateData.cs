﻿using System;
using WingsMob.MiniGame.TileCat.TileCat;

namespace WingsMob.MiniGame
{
    [Serializable]
    public class TemplateData<T, U>
    {
        public T Key;
        public U Value;
    }

    [Serializable]
    public class MiniGameDictionary : TemplateData<CatItemType, int>
    {
 
    }
}