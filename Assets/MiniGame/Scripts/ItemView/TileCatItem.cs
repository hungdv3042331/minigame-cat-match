﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WingsMob.MiniGame.TileCat.TileCat
{
    public class TileCatItem : MonoBehaviour
    {
        [SerializeField] private CatItemType m_catItemType;
        public CatItemType CatItemType => m_catItemType;

        private SpriteRenderer m_spriteRenderer;

        public void SetTileVisual(CatItemType type, Sprite sprite)
        {
            m_spriteRenderer = GetComponent<SpriteRenderer>();
            m_catItemType = type;
            m_spriteRenderer.sprite = sprite;
        }
    }

    //example
    public enum CatItemType : int
    {
        None, Type_1, Type_2, Type_3, Type_4
    }
}
