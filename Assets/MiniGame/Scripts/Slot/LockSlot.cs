﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WingsMob.MiniGame.TileCat
{
    [RequireComponent(typeof(Button))]
    public class LockSlot : MonoBehaviour
    {
        private Button m_button;

        private void Start()
        {
            m_button = GetComponent<Button>();
            m_button.onClick.AddListener(OnClickUnlockSlot);
        }

        private void OnClickUnlockSlot()
        {
            gameObject.SetActive(false);
        }
    }
}