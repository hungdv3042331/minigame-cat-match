﻿using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WingsMob.MiniGame.TileCat.Controller;
using WingsMob.MiniGame.TileCat.TileCat;

namespace WingsMob.MiniGame.TileCat
{
    public class Slot : MonoBehaviour
    {
        [ReadOnly, SerializeField] private ClickableItem m_item;
        [SerializeField] private LockSlot m_lockSlot;

        public bool IsEmpty => m_item == null;
        public ClickableItem Item => m_item;

        public bool IsLocked
        {
            get
            {
                return m_lockSlot != null && m_lockSlot.gameObject.activeSelf;
            }
        }

        public void AssignItem(ClickableItem newItem)
        {
            m_item = newItem;
        }

        public void RemoveItem()
        {
            m_item = null;
        }

        public CatItemType GetItemType()
        {
            return m_item.GetComponent<TileCatItem>().CatItemType;
        }

        public void ResetSlot()
        {
            m_item = null;
            StopAllCoroutines();
            DOTween.KillAll();
        }
    }
}