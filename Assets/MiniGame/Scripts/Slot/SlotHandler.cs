﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using WingsMob.MiniGame.TileCat.Controller;
using WingsMob.MiniGame.TileCat.TileCat;

namespace WingsMob.MiniGame.TileCat
{
    public class SlotHandler : MonoBehaviour
    {
        [SerializeField] List<Slot> m_slots;

        private Slot GetAvailableSlot()
        {
            for (int i = 0; i < m_slots.Count; i++)
            {
                if (m_slots[i].IsEmpty && !m_slots[i].IsLocked)
                {
                    return m_slots[i];
                }
            }
            return null;
        }

        public Slot GetAvailableSlot(CatItemType itemType)
        {
            var emptySlot = m_slots.Find(slot => slot.IsEmpty && !slot.IsLocked);
            if (emptySlot == null) return null;
            int newSlotIndex = 0;
            for (int i = 0; i < m_slots.Count - 1; i++)
            {
                if (!m_slots[i].IsEmpty && m_slots[i].GetItemType() == itemType && !m_slots[i].IsLocked)
                {
                    newSlotIndex = i + 1;
                }
            }
            if (newSlotIndex == 0) return GetAvailableSlot();
            for (int i = m_slots.Count - 1; i > newSlotIndex; i--)
            {
                ClickableItem oldItemInSlot = m_slots[i - 1].Item;
                if (oldItemInSlot != null && oldItemInSlot.GetComponent<TileCatItem>().CatItemType != itemType)
                {
                    Slot newSlot = m_slots[i];
                    oldItemInSlot.DirectAssignSlot(newSlot);
                }
            }
            return m_slots[newSlotIndex].IsEmpty ? m_slots[newSlotIndex] : GetAvailableSlot();
        }

        private int GetItemAmount(CatItemType catItemType)
        {
            int result = 0;
            for (int i = 0; i < m_slots.Count; i++)
            {
                if (!m_slots[i].IsEmpty && m_slots[i].GetItemType() == catItemType)
                {
                    result++;
                }
            }
            return result;
        }

        public void CheckMatchItems()
        {
            for (int i = 1; i < m_slots.Count - 1; i++)
            {
                if (m_slots[i].IsEmpty || m_slots[i - 1].IsEmpty || m_slots[i + 1].IsEmpty ||
                    m_slots[i].IsLocked || m_slots[i - 1].IsLocked || m_slots[i + 1].IsLocked) break;
                if (m_slots[i].GetItemType() == m_slots[i - 1].GetItemType()
                    && m_slots[i].GetItemType() == m_slots[i + 1].GetItemType())
                {
                    Transform middleItemTransform = m_slots[i].transform;
                    Slot middleSlot = m_slots[i];
                    Slot leftSlot = m_slots[i - 1];
                    Slot rightSlot = m_slots[i + 1];
                    var itemType = m_slots[i].GetItemType();
                    void MatchItemCallback()
                    {
                        MoveMissMatchItemsToEmptySlot();
                    }
                    m_slots[i + 1].Item.OnMatch(middleItemTransform);
                    m_slots[i - 1].Item.OnMatch(middleItemTransform);
                    m_slots[i].Item.OnMatch(middleItemTransform, MatchItemCallback);
                    break; //break to make sure just match 3 items
                }
            }
        }

        /// <summary>
        /// move all miss match item in all slots to empty slots
        /// </summary>
        private void MoveMissMatchItemsToEmptySlot()
        {
            int startEmptyIndex = -1;
            for (int i = 0; i < m_slots.Count; i++)
            {
                if (m_slots[i].IsEmpty && !m_slots[i].IsLocked)
                {
                    startEmptyIndex = i;
                    break;
                }
            }
            //Common.Log("startEmptyIndex: " + startEmptyIndex);
            for (int i = startEmptyIndex + 3; i < m_slots.Count; i++)
            {
                if (!m_slots[i].IsEmpty)
                {
                    ClickableItem oldItemInSlot = m_slots[i].Item;
                    Slot newSlot = m_slots[i - 3];
                    oldItemInSlot.DirectAssignSlot(newSlot);
                }
            }
        }
    }
}