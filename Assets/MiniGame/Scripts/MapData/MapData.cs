﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WingsMob.MiniGame.TileCat
{
    [Serializable]
    public class TileData
    {
        public float x;
        public float y;
        public int type;

        public TileData(float x, float y, int type)
        {
            this.x = x;
            this.y = y;
            this.type = type;
        }
    }

    [Serializable]
    public class MapData
    {
        public int level;
        public int mapsizeX;
        public int mapsizeY;
        public float tileSize;
        public float tileSpacing;
        public List<TileData> tiles;

        public MapData()
        {
            tiles = new List<TileData>();
        }
    }
}