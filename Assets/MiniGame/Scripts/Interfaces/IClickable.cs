﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WingsMob.MiniGame.TileCat.Interface
{
    public interface IClickable 
    {
        void OnClick();
        void OnDrag(Vector2 position);
        void OnDrop();
    }
}