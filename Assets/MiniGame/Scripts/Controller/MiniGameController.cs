﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WingsMob.MiniGame.TileCat.TileCat;

namespace WingsMob.MiniGame.TileCat.Controller
{
    public class MiniGameController : SingletonMono<MiniGameController>
    {
        [SerializeField] SlotHandler m_slotHandler;
        public SlotHandler SlotHandler => m_slotHandler;

        [Title("List items: ")]
        [SerializeField] List<TileCatItem> m_catItems;
        [SerializeField] Queue<TileCatItem> m_catItemQueue = new Queue<TileCatItem>();

        [Title("Canvas: ")]
        public Canvas m_canvas;

        [Title("Map container: ")]
        [SerializeField] MapContainer m_mapContainer;
        [SerializeField] TileCatItem m_tileCatPrefab;

        [Title("Sprite Items: ")]
        [SerializeField] Sprite[] m_spriteItems;

        private Dictionary<CatItemType, int> m_itemCountDict = new Dictionary<CatItemType, int>();

        [HideInInspector] public Camera m_mainCamera;

        public int GameLevel { get; private set; }

        private void Start()
        {
            m_mainCamera = Camera.main;
            GameLevel = 1;
            CreateMap();
        }

        private void CreateMap()
        {
            if (m_catItems != null) m_catItems.Clear();
            m_catItems = new List<TileCatItem>();
            var textAsset = Resources.Load($"mapData_{GameLevel}").ToString();
            var mapData = JsonUtility.FromJson<MapData>(textAsset);

            int mapsizeX = mapData.mapsizeX;
            int mapsizeY = mapData.mapsizeY;
            float tileSize = mapData.tileSize;
            float tileSpacing = mapData.tileSpacing;
            for (int x = 0; x < mapsizeX; x++)
            {
                for (int y = 0; y < mapsizeY; y++)
                {
                    Vector3 tileCatPosition = new Vector3(x * (tileSize + tileSpacing), y * (tileSize + tileSpacing), 0);
                    TileCatItem tileCatItem = Instantiate(m_tileCatPrefab);
                    tileCatItem.transform.SetPositionAndRotation(tileCatPosition, Quaternion.identity);
                    tileCatItem.transform.parent = m_mapContainer.transform;
                    tileCatItem.SetTileVisual((CatItemType)mapData.tiles[x * mapsizeY + y].type, m_spriteItems[mapData.tiles[x * mapsizeY + y].type - 1]);
#if UNITY_EDITOR
                    tileCatItem.gameObject.name = $"TileCat ({x} ; {y})";
#endif
                    m_catItems.Add(tileCatItem);
                    m_catItemQueue.Enqueue(tileCatItem);
                }
            }

            float totalX = 0f;
            for (int x = 0; x < mapsizeX; x++)
            {
                totalX += x * (tileSize + tileSpacing);
            }
            float boardWorldPositionX = totalX / (float)mapsizeX;
            float totalY = 0f;
            for (int y = 0; y < mapsizeY; y++)
            {
                totalY += y * (tileSize + tileSpacing);
            }
            float boardWorldPositionY = totalY / (float)mapsizeY;
            m_mapContainer.transform.position = new Vector2(-boardWorldPositionX, -boardWorldPositionY);
        }

        public void AddItemCountInSlot(CatItemType itemType)
        {
            if (m_itemCountDict.ContainsKey(itemType))
            {
                m_itemCountDict[itemType]++;
            }
            else
            {
                m_itemCountDict.Add(itemType, 1);
            }

            if (m_itemCountDict[itemType] % 3 == 0)
            {
                m_slotHandler.CheckMatchItems();
            }
        }

        public void ClearMatchItem(CatItemType itemType)
        {
            if (m_itemCountDict.ContainsKey(itemType))
            {
                m_itemCountDict[itemType] = 0;
            }
        }
    }
}