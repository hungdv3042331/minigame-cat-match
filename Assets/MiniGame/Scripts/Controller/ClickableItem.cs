﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using WingsMob.MiniGame.TileCat.Interface;
using WingsMob.MiniGame.TileCat.TileCat;
using UnityEngine.UIElements;
using Cysharp.Threading.Tasks;

namespace WingsMob.MiniGame.TileCat.Controller
{
    public class ClickableItem : MonoBehaviour, IClickable
    {
        [SerializeField] private SlotHandler m_slotHandler;
        [SerializeField] private Slot m_currentSlot;
        private CatItemType m_itemType;
        private Vector2 m_initPosition;
        private Collider2D m_collider2D;

        private void Start()
        {
            m_slotHandler = MiniGameController.Instance.SlotHandler;
            m_itemType = GetComponent<TileCatItem>().CatItemType;
            m_initPosition = transform.position;
            m_collider2D = GetComponent<Collider2D>();
            m_collider2D.enabled = true;
        }

        public void OnClick()
        {
            if (m_currentSlot != null)
            {
                return;
            }

            var availableSlot = m_slotHandler.GetAvailableSlot(m_itemType);
            if (availableSlot != null)
            {
                Common.Log("item clicked:" + gameObject.name);
                m_collider2D.enabled = false;
                m_currentSlot = availableSlot;
                m_currentSlot.AssignItem(this);

                var worldPosition = MiniGameController.Instance.m_mainCamera.ScreenToWorldPoint(availableSlot.transform.position);
                worldPosition.z = 0f;
                transform.DOMove(worldPosition, 1f).OnComplete(() =>
                {
                    MiniGameController.Instance.AddItemCountInSlot(m_itemType);
                });
            }
            else
            {
                Common.LogWarning(" (availableSlot == null)");
            }
        }

        public void OnDrag(Vector2 position)
        {

        }

        public void OnDrop()
        {

        }

        public void DirectAssignSlot(Slot newSlot)
        {
            m_currentSlot.RemoveItem();
            m_currentSlot = newSlot;
            m_currentSlot.AssignItem(this);
            var worldPosition = MiniGameController.Instance.m_mainCamera.ScreenToWorldPoint(newSlot.transform.position);
            worldPosition.z = 0f;
            transform.DOMove(worldPosition, 1f);
        }

        public void OnMatch(Transform middleItemTransform, UnityAction callback = null)
        {
            m_currentSlot.RemoveItem();
            m_currentSlot = null;
            var worldPosition = MiniGameController.Instance.m_mainCamera.ScreenToWorldPoint(middleItemTransform.position);
            worldPosition.z = 0f;
            transform.DOMove(worldPosition, 0.5f).OnComplete(() =>
            {
                if (m_currentSlot.Item == this) //avoid case remove 'item in slot' removed when moving and 'item in slot' != this item
                {
                    m_currentSlot.RemoveItem();
                    m_currentSlot = null;
                }
            });
        }

        public void ResetItem()
        {
            transform.position = m_initPosition;
            gameObject.SetActive(true);
            m_currentSlot = null;
            StopAllCoroutines();
            DOTween.KillAll();
        }
    }
}
