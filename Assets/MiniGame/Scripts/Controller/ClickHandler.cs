﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WingsMob.MiniGame.TileCat.Interface;

namespace WingsMob.MiniGame.TileCat.Controller
{
    public class ClickHandler : MonoBehaviour
    {
        [SerializeField] LayerMask m_clickableLayer;
        private Camera m_mainCamera;
        private IClickable m_clickedObject;
        private bool m_isClicked;

        private void Start()
        {
            m_mainCamera = Camera.main;
        }

        void Update()
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonUp(0))
            {
                if (m_isClicked)
                {
                    m_isClicked = false;
                    m_clickedObject.OnDrop();
                    m_clickedObject = null;
                    return;
                }
            }
            if (m_isClicked)
            {
                Vector3 mousePos = m_mainCamera.ScreenToWorldPoint(Input.mousePosition);
                m_clickedObject.OnDrag(mousePos);
                return;
            }
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit2D hit;
                Vector2 mousePos = m_mainCamera.ScreenToWorldPoint(Input.mousePosition);
                if (hit = Physics2D.Raycast(mousePos, Vector2.zero, 0f, m_clickableLayer))
                {
                    m_clickedObject = hit.collider.GetComponent(typeof(IClickable)) as IClickable;
                    m_clickedObject.OnClick();
                }
            }
#endif

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            if (Input.touchCount == 1)
            {
                if (m_isClicked)
                {
                    m_isClicked = false;
                    m_clickedObject.OnDrop();
                    m_clickedObject = null;
                    return;
                }
            }
            if (m_isClicked)
            {
                Vector3 mousePos = m_mainCamera.ScreenToWorldPoint(Input.touches[0].position);
                m_clickedObject.OnDrag(mousePos);
                return;
            }
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                RaycastHit2D hit;
                Vector2 mousePos = m_mainCamera.ScreenToWorldPoint(Input.touches[0].position);
                if (hit = Physics2D.Raycast(mousePos, Vector2.zero, 0f, m_clickableLayer))
                {
                    m_clickedObject = hit.collider.GetComponent(typeof(IClickable)) as IClickable;
                    m_clickedObject.OnClick();
                }
            } 
#endif
        }
    }
}