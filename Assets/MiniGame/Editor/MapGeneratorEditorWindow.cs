﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using WingsMob.MiniGame.TileCat.TileCat;

namespace WingsMob.MiniGame.TileCat
{
    public class MapGeneratorEditorWindow : EditorWindow
    {
        private int mapLevel = 1;
        private int mapSizeX = 6;
        private int mapSizeY = 6;
        private float tileSize = 0.7f; // Size of each tile
        private float tileSpacing = 0.1f; // Spacing between tiles
        private int levelToDeleteData = 1;

        private Vector2 scrollViewVector;

        private ItemEditor[] m_itemEditors;

        public Texture textureCatType_1;
        public Texture textureCatType_2;
        public Texture textureCatType_3;
        public Texture textureCatType_4;

        private CatItemType m_tempItemType;

        [MenuItem("WingsMob/MiniGame/Map Generator")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(MapGeneratorEditorWindow));
        }

        private void OnFocus()
        {
            m_itemEditors = new ItemEditor[mapSizeX * mapSizeY];
            textureCatType_1 = AssetDatabase.LoadAssetAtPath<Texture>("Assets/MiniGame/Textures/PNG/dotBlue.png");
            textureCatType_2 = AssetDatabase.LoadAssetAtPath<Texture>("Assets/MiniGame/Textures/PNG/dotGreen.png");
            textureCatType_3 = AssetDatabase.LoadAssetAtPath<Texture>("Assets/MiniGame/Textures/PNG/dotRed.png");
            textureCatType_4 = AssetDatabase.LoadAssetAtPath<Texture>("Assets/MiniGame/Textures/PNG/dotYellow.png");
            for (int i = 0; i < m_itemEditors.Length; i++)
            {
                ItemEditor itemEditor = new ItemEditor();
                itemEditor.m_type = CatItemType.None;
                itemEditor.m_texture = GenerateTextureByType(CatItemType.None);
                m_itemEditors[i] = itemEditor;
            }
        }

        private void OnGUI()
        {
            GUILayout.Label("Map Generator", EditorStyles.boldLabel);

            mapLevel = EditorGUILayout.IntField("Level:", mapLevel);
            mapSizeX = EditorGUILayout.IntField("Map Size X:", mapSizeX);
            mapSizeY = EditorGUILayout.IntField("Map Size Y:", mapSizeY);
            tileSize = EditorGUILayout.FloatField("Tile Size", tileSize);
            tileSpacing = EditorGUILayout.FloatField("Tile Spacing", tileSpacing);

            scrollViewVector = GUI.BeginScrollView(new Rect(25, 45, position.width - 30, position.height), scrollViewVector, new Rect(0, 0, 400, 1600));
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Generate Map", GUILayout.Width(120), GUILayout.Height(20)))
            {
                GenerateMap();
            }
            if (GUILayout.Button("Clear Map", GUILayout.Width(120), GUILayout.Height(20)))
            {
                ClearMap();
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
            levelToDeleteData = EditorGUILayout.IntField("Delete level:", levelToDeleteData);
            if (GUILayout.Button("Delete Level", GUILayout.Width(120), GUILayout.Height(20)))
            {
                DeleteLevelData(levelToDeleteData);
            }
            GUICatItem();
            GUILayout.Space(20);
            GUIGameField();

            GUI.EndScrollView();
        }

        private void GenerateMap()
        {
            // Clear any existing map
            ClearMap();

            // Create map data
            MapData mapData = new MapData();
            mapData.level = mapLevel;
            mapData.mapsizeX = mapSizeX;
            mapData.mapsizeY = mapSizeY;
            mapData.tileSize = tileSize;
            mapData.tileSpacing = tileSpacing;

            // Generate the map
            for (int x = 0; x < mapSizeX; x++)
            {
                for (int y = 0; y < mapSizeY; y++)
                {
                    Vector3 tilePosition = new Vector3(x * (tileSize + tileSpacing), y * (tileSize + tileSpacing), 0);
                    mapData.tiles.Add(new TileData(tilePosition.x, tilePosition.y, (int)m_itemEditors[x * mapSizeY + y].m_type));
                }
            }

            // Convert map data to JSON
            string jsonMapData = JsonUtility.ToJson(mapData);

            // Save JSON to a file (in the Assets folder)
            string filePath = $"Assets/MiniGame/Resources/mapData_{mapData.level}.json";
            File.WriteAllText(filePath, jsonMapData);

            Debug.Log("Map generated and saved to JSON.");
        }

        private Texture GenerateTextureByType(CatItemType catItemType)
        {
            switch (catItemType)
            {
                case CatItemType.Type_1:
                    return textureCatType_1;
                case CatItemType.Type_2:
                    return textureCatType_2;
                case CatItemType.Type_3:
                    return textureCatType_3;
                case CatItemType.Type_4:
                    return textureCatType_4;
                default:
                    return textureCatType_1;
            }
        }

        private void GUICatItem()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Cat Items: ", EditorStyles.boldLabel);
            GUILayout.Space(20);
            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            GUI.color = new Color(1, 1, 1, 1f);
            GUILayout.Label("Cat Item 1", EditorStyles.boldLabel);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUI.color = new Color(0.8f, 1, 1, 1f);
            if (GUILayout.Button(textureCatType_1, new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) }))
            {
                m_tempItemType = CatItemType.Type_1;
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(20);
            GUILayout.BeginHorizontal();
            GUI.color = new Color(1, 1, 1, 1f);
            GUILayout.Label("Cat Item 2", EditorStyles.boldLabel);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUI.color = new Color(0.8f, 1, 1, 1f);
            if (GUILayout.Button(textureCatType_2, new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) }))
            {
                m_tempItemType = CatItemType.Type_2;
            }
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUI.color = new Color(1, 1, 1, 1f);
            GUILayout.Label("Cat Item 3", EditorStyles.boldLabel);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUI.color = new Color(0.8f, 1, 1, 1f);
            if (GUILayout.Button(textureCatType_3, new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) }))
            {
                m_tempItemType = CatItemType.Type_3;
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(20);
            GUILayout.BeginHorizontal();
            GUI.color = new Color(1, 1, 1, 1f);
            GUILayout.Label("Cat Item 4", EditorStyles.boldLabel);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUI.color = new Color(0.8f, 1, 1, 1f);
            if (GUILayout.Button(textureCatType_4, new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) }))
            {
                m_tempItemType = CatItemType.Type_4;
            }
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }

        private void GUIGameField()
        {
            GUILayout.Label("GameField: ", EditorStyles.boldLabel);
            GUILayout.BeginVertical();
            for (int row = 0; row < mapSizeX; row++)
            {
                GUILayout.BeginHorizontal();
                for (int col = 0; col < mapSizeY; col++)
                {
                    Color squareColor = new Color(0.8f, 0.8f, 0.8f);
                    var imageButton = new object();
                    if (m_itemEditors[row * mapSizeY + col].m_type == CatItemType.Type_1)
                    {
                        imageButton = textureCatType_1;
                        squareColor = new Color(0.8f, 0.8f, 0.8f);
                    }
                    else if (m_itemEditors[row * mapSizeY + col].m_type == CatItemType.Type_2)
                    {
                        imageButton = textureCatType_2;
                        squareColor = new Color(0.8f, 0.8f, 0.8f);
                    }
                    else if (m_itemEditors[row * mapSizeY + col].m_type == CatItemType.Type_3)
                    {
                        imageButton = textureCatType_3;
                        squareColor = new Color(0.8f, 0.8f, 0.8f);
                    }
                    else if (m_itemEditors[row * mapSizeY + col].m_type == CatItemType.Type_4)
                    {
                        imageButton = textureCatType_4;
                        squareColor = new Color(0.8f, 0.8f, 0.8f);
                    }
                    GUI.color = squareColor;
                    if (GUILayout.Button(imageButton as Texture, GUILayout.Width(50), GUILayout.Height(50)))
                    {
                        if (Event.current.button == 0)
                            SetCatTile(row, col);
                        else
                            SetCatTileEmpty(row, col);
                    }
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }

        private void SetCatTile(int x, int y)
        {
            m_itemEditors[x * mapSizeY + y].m_type = m_tempItemType;
            m_itemEditors[x * mapSizeY + y].m_texture = GenerateTextureByType(m_tempItemType);
        }

        private void SetCatTileEmpty(int x, int y)
        {
            m_itemEditors[x * mapSizeY + y].m_type = CatItemType.None;
            m_itemEditors[x * mapSizeY + y].m_texture = null;
        }

        private void ClearMap()
        {
            // Destroy all existing tiles
            GameObject[] existingTiles = GameObject.FindGameObjectsWithTag("TileCat");
            foreach (GameObject tile in existingTiles)
            {
                DestroyImmediate(tile);
            }
        }

        private void DeleteLevelData(int mapLevel)
        {
            string filePath = $"Assets/MiniGame/Resources/mapData_{mapLevel}.json";
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }
    }
}