﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WingsMob.MiniGame.TileCat.TileCat;

namespace WingsMob.MiniGame.TileCat
{
    public class ItemEditor 
    {
        public CatItemType m_type;
        public Texture m_texture;
    }
}
